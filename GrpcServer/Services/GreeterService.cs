using System;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace GrpcServer
{
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        public GreeterService(ILogger<GreeterService> logger)
        {
            _logger = logger;
        }

        public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
        {

            string[] str_arr = request.Name.ToString().Split('/');
            int[] numbers = Array.ConvertAll(str_arr, int.Parse);

            int zi = numbers[0];
            int luna = numbers[1];
            int anul = numbers[2];

            string line;
            string sign="";

            System.IO.StreamReader file =
                new System.IO.StreamReader(@"C:\Users\mihae\OneDrive\Desktop\Tema1-Cloud\GrpcServer\file.txt");

            for (int index = 0; index < 12; index++)
            {
                line = file.ReadLine();
                System.Console.WriteLine(line);
                string[] text = line.ToString().Split(' ');
                sign = text[0];
                string start = text[1];
                string end = text[2];


                string[] startDate = start.Split('/');

                string[] endDate = end.Split('/');

              
                int[] number = Array.ConvertAll(startDate, int.Parse);
                int[] number2 = Array.ConvertAll(endDate, int.Parse);

                int ziStart = number[0];
                int lunaStart= number[1];
                int ziEnd = number2[0];
                int lunaEnd = number2[1];

                if((ziStart<=zi && lunaStart==luna)||(luna==lunaEnd&& zi<=ziEnd))
                {
                    break;

                }
            }


            file.Close();


            return Task.FromResult(new HelloReply
            {
                Message = "Your zodiac sign is " + sign
            });

        }
    }
}

﻿using Grpc.Net.Client;
using GrpcServer;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GrpcClient
{
    class Program
    {
        private static bool Verify(string date)
        {

            Regex r = new Regex(@"\d{2}/\d{2}/\d{4}");

            if (r.Match(date).Success)
                return true;
            else return false;
        }

       
        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Greeter.GreeterClient(channel);

            Console.WriteLine("Please write your birthday: ");
            string input = Console.ReadLine();

            bool ok = false;

            while (ok == false)
            {
                if (Verify(input) == false)
                {
                    Console.WriteLine("The date isn't correct, please write again!");
                    input = Console.ReadLine();
                }

                else
                {
                    ok = true;
                    var Reply = await client.SayHelloAsync(new HelloRequest { Name = input });
                    Console.WriteLine(Reply.Message);
                }

            }

            Console.ReadKey();
        }
    }
}
